FROM openjdk:8
ADD target/docker-spring-patients.jar docker-spring-patients.jar
EXPOSE 8082
ENTRYPOINT ["java", "-jar", "docker-spring-patients.jar"]