package com.softtek.msacademy.febrerohospitalpatients.repository;

import com.softtek.msacademy.febrerohospitalpatients.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {}
