package com.softtek.msacademy.febrerohospitalpatients.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "states")
public class State implements Serializable {
    State(){}
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_state;
    @Column(name = "name")
    private String name;

    public Integer getId_state() {
        return id_state;
    }

    public void setId_state(Integer id_state) {
        this.id_state = id_state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
