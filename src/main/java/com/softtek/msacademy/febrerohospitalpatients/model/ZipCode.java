package com.softtek.msacademy.febrerohospitalpatients.model;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "zip_codes")
//@EntityListeners(AuditingEntityListener.class)
public class ZipCode{
    ZipCode(){}
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_zip_code;
    @Column(name = "code")
    private String code;

    public Integer getId_zip_code() {
        return id_zip_code;
    }

    public void setId_zip_code(Integer id_zip_code) {
        this.id_zip_code = id_zip_code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
