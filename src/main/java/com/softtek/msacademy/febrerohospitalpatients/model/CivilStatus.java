package com.softtek.msacademy.febrerohospitalpatients.model;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "civil_status")
public class CivilStatus  implements  Serializable{
    CivilStatus(){}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_civil_status;
    @Column(name = "status")
    private String status;

    public int getId_civil_status() {
        return id_civil_status;
    }

    public void setId_civil_status(int id_civil_status) {
        this.id_civil_status = id_civil_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
