package com.softtek.msacademy.febrerohospitalpatients.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "nationalities")
public class Nationality implements Serializable{
    Nationality(){}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_nationality;
    @Column(name = "nationality")
    private String nationality;

    public int getId_nationality() {
        return id_nationality;
    }

    public void setId_nationality(int id_nationality) {
        this.id_nationality = id_nationality;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
}
