package com.softtek.msacademy.febrerohospitalpatients.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "fiscal_data")
@EntityListeners(AuditingEntityListener.class)
public class FiscalData implements Serializable {
    FiscalData(){}
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_fiscal_data;
    @Column(name = "rfc")
    private String rfc;
    @Column(name = "curp")
    private String curp;

    @JoinColumn(name = "address")
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Address address;

    @JoinColumn(name = "zip_code", nullable = true)
    @ManyToOne(optional = false,cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private ZipCode zipCode;

    @JoinColumn(name = "state")
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private State state;

    public Integer getId_fiscal_data() {
        return id_fiscal_data;
    }

    public void setId_fiscal_data(Integer id_fiscal_data) {
        this.id_fiscal_data = id_fiscal_data;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ZipCode getZipCode() {
        return zipCode;
    }

    public void setZipCode(ZipCode zipCode) {
        this.zipCode = zipCode;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
