package com.softtek.msacademy.febrerohospitalpatients.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "address")
public class Address implements Serializable{
    Address (){}
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_address;
    @Column(name = "name_street")
    private String name_street;
    @Column(name = "nummber_exterior")
    private Integer number_exterior;
    @Column(name = "num_internal")
    private Integer number_internal;


    public Integer getId_address() {
        return id_address;
    }

    public void setId_address(Integer id_address) {
        this.id_address = id_address;
    }

    public String getName_street() {
        return name_street;
    }

    public void setName_street(String name_street) {
        this.name_street = name_street;
    }

    public Integer getNumber_exterior() {
        return number_exterior;
    }

    public void setNumber_exterior(Integer number_exterior) {
        this.number_exterior = number_exterior;
    }

    public Integer getNumber_internal() {
        return number_internal;
    }

    public void setNumber_internal(Integer number_internal) {
        this.number_internal = number_internal;
    }
}
