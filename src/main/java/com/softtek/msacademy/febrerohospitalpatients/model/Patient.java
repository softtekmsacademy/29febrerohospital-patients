package com.softtek.msacademy.febrerohospitalpatients.model;
import javax.persistence.*;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "patients")
@EntityListeners(AuditingEntityListener.class)
public class Patient implements Serializable{
    //Empty Constructor
    public Patient(){}

    //Main fields of table
    @Id
    @GeneratedValue()
    private Long nss;

    @Column(name = "first_name", nullable = false)
    private String first_name;
    @Column(name = "last_name", nullable = false)
    private String last_name;
    @Column(name = "second_last_name", nullable = false)
    private String second_last_name;
    @Column(name = "birth_date", nullable = false)
    private Date birth_date;
    @Column(name = "gender", nullable = false)
    private String gender;

    //Foreign Keys
    @JoinColumn(name = "fiscal_data")
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private FiscalData fromFiscalData;

    @JoinColumn(name = "blood_group")
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private BloodGroup bloodGroup;

    @JoinColumn(name = "birthplace")
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private State state;

    @JoinColumn(name = "nationality")
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Nationality nationality;

    @JoinColumn(name = "religion")
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Religion religion;

    @JoinColumn(name = "civil_status")
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private CivilStatus civilStatus;

    @Column(name = "id_health_record")
    private Integer health_record;

    public Long getNss() {
        return nss;
    }

    //GET & SET
    public void setNss(Long nss) {
        this.nss = nss;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getSecond_last_name() {
        return second_last_name;
    }

    public void setSecond_last_name(String second_last_name) {
        this.second_last_name = second_last_name;
    }

    public Date getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(Date birth_date) {
        this.birth_date = birth_date;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public FiscalData getFromFiscalData() {
        return fromFiscalData;
    }

    public void setFromFiscalData(FiscalData fromFiscalData) {
        this.fromFiscalData = fromFiscalData;
    }

    public BloodGroup getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(BloodGroup bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Nationality getNationality() {
        return nationality;
    }

    public void setNationality(Nationality nationality) {
        this.nationality = nationality;
    }

    public Religion getReligion() {
        return religion;
    }

    public void setReligion(Religion religion) {
        this.religion = religion;
    }

    public CivilStatus getCivilStatus() {
        return civilStatus;
    }

    public void setCivilStatus(CivilStatus civilStatus) {
        this.civilStatus = civilStatus;
    }

    public Integer getHealth_record() {
        return health_record;
    }

    public void setHealth_record(Integer health_record) {
        this.health_record = health_record;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "nss=" + nss +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", second_last_name='" + second_last_name + '\'' +
                ", birth_date=" + birth_date +
                ", gender='" + gender + '\'' +
                ", fromFiscalData=" + fromFiscalData +
                ", bloodGroup=" + bloodGroup +
                ", birth_place=" + state +
                ", nationality=" + nationality +
                ", religion=" + religion +
                ", civilStatus=" + civilStatus +
                ", health_record=" + health_record +
                '}';
    }
}

