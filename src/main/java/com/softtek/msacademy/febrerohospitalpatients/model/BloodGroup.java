package com.softtek.msacademy.febrerohospitalpatients.model;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "blood_group")
public class BloodGroup implements Serializable{
    BloodGroup (){}
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_blood_group;
    @Column(name = "description")
    private String description;

    public int getId_blood_group() {
        return id_blood_group;
    }

    public void setId_blood_group(int id_blood_group) {
        this.id_blood_group = id_blood_group;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
