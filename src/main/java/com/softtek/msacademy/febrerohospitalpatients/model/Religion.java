package com.softtek.msacademy.febrerohospitalpatients.model;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "religions")
public class Religion implements Serializable{
    Religion(){}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_religion;
    @Column(name = "religion")
    private String religion;


    public int getId_religion() {
        return id_religion;
    }

    public void setId_religion(int id_religion) {
        this.id_religion = id_religion;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }
}
