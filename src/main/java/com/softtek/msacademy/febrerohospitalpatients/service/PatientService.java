package com.softtek.msacademy.febrerohospitalpatients.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.softtek.msacademy.febrerohospitalpatients.model.Patient;
import com.softtek.msacademy.febrerohospitalpatients.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PatientService {
    @Autowired
    private PatientRepository repository;

    public Patient create(Patient patient) {
        return repository.save(patient);
    }

    @HystrixCommand
    public Optional<Patient> getPatientsByOrg(Long nss){
        return repository.findById(nss);
    }
}
