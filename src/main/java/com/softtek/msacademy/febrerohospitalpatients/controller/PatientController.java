package com.softtek.msacademy.febrerohospitalpatients.controller;

import com.softtek.msacademy.febrerohospitalpatients.exeption.ResourceNotFoundException;
import com.softtek.msacademy.febrerohospitalpatients.model.Patient;
import com.softtek.msacademy.febrerohospitalpatients.repository.PatientRepository;
import com.softtek.msacademy.febrerohospitalpatients.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import com.netflix.discovery.EurekaClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import javax.validation.Valid;
import java.util.Date;

@RefreshScope
@RestController
@RequestMapping("/administration")//Path of index module
public class PatientController {
    //call of repository & service of patient from package service
    @Autowired
    private PatientRepository patientRepository;
    private PatientService service;

    @Autowired
    private EurekaClient eurekaClient;

    //Method for return all patients
    @RequestMapping(value = "/patient", method = RequestMethod.GET)
    public ResponseEntity List() {
        return new ResponseEntity(patientRepository.findAll(), HttpStatus.OK);
    }

    //Method for return one patient for his/her Social Security Number
    @RequestMapping(value = "/patient/{nss}", method = RequestMethod.GET)
    public ResponseEntity patientById(@PathVariable(value = "nss") long nss){
        return new ResponseEntity(patientRepository.findById(nss), HttpStatus.OK);
    }

    //Method for insert a patient in data base using a body type json
    @RequestMapping(value = "/patient/new", method = RequestMethod.POST)
    public ResponseEntity createPatient(@Valid @RequestBody Patient patient){
        Patient newPatient = service.create(patient);
        return new ResponseEntity(newPatient, HttpStatus.CREATED);
    }

    //Method for update patient data
    @PutMapping("/patient/{nss}")
    public ResponseEntity<Patient> updateUser(
            @PathVariable(value = "nss") Long nss, @Valid @RequestBody Patient patientDetails)
            throws ResourceNotFoundException {
        Patient patient =
                patientRepository
                        .findById(nss)
                        .orElseThrow(() -> new ResourceNotFoundException("User not found on :: ","  " , nss));
        patient.setFirst_name(patientDetails.getFirst_name());
        patient.setLast_name(patientDetails.getLast_name());
        patient.setSecond_last_name(patientDetails.getSecond_last_name());
        patient.setBirth_date(new Date());
        patient.setGender(patientDetails.getGender());
        patient.setFromFiscalData(patientDetails.getFromFiscalData());
        patient.setBloodGroup(patientDetails.getBloodGroup());
        patient.setState(patientDetails.getState());
        patient.setNationality(patientDetails.getNationality());
        patient.setReligion(patientDetails.getReligion());
        patient.setCivilStatus(patientDetails.getCivilStatus());

        final Patient patientUpdate = patientRepository.save(patient);
        return ResponseEntity.ok(patientUpdate);
    }
}
